{
  description = "The next gen ls command";
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixpkgs-unstable;
    alejandra = {
      url = github:kamadorueda/alejandra;
      inputs.nixpkgs.follows = "nixpkgs";
    };
    crane = {
      url = github:ipetkov/crane;
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs =
    inputs @ { self
    , nixpkgs
    , ...
    }:
      with builtins; let
        std = nixpkgs.lib;
        systems = attrNames inputs.crane.lib; #std.systems.flakeExposed;
      in
      {
        formatter = std.mapAttrs (system: pkgs: pkgs.default) inputs.alejandra.packages;
        overlays = std.genAttrs systems (system:
          let
            crn = inputs.crane.lib.${system};
          in
          final: prev: {
            lsd = crn.buildPackage {
              src = ./.;
            };
          });
        packages =
          std.mapAttrs
            (system: overlay:
              let
                pkgs = import nixpkgs {
                  localSystem = system;
                  crossSystem = system;
                  overlays = [ overlay ];
                };
              in
              {
                inherit (pkgs) lsd;
                default = pkgs.lsd;
              })
            self.overlays;
      };
}
